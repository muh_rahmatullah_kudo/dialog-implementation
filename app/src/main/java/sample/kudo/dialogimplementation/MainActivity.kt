package sample.kudo.dialogimplementation

import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_dialog.view.*

class MainActivity : AppCompatActivity(), ItemAdapter.ItemClickListener {

    val dialogList: MutableList<Dialog> = mutableListOf()

    lateinit var adapter: ItemAdapter

    val dialogTag = "dialog-tag"

    override fun onItemClick(dialog: Dialog) {
        invokeDialog(dialog)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        provideListOfDialog()

        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvListDialog.layoutManager = linearLayoutManager
        rvListDialog.adapter = adapter

    }

    fun provideListOfDialog() {
        var dialog = Dialog(
            "R.style.AppTheme_Kudo_Dialog_Alert_Confirmation",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_Confirmation
        )
        dialogList.add(dialog)
        dialog = Dialog(
            "AppTheme_Kudo_Dialog_Alert",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert
        )
        dialogList.add(dialog)
        dialog = Dialog(
            "AppTheme_Kudo_Dialog_Alert_Error",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_Error
        )
        dialogList.add(dialog)
        dialog = Dialog(
            "AppTheme_Kudo_Dialog_Alert_HalfButton",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_HalfButton
        )
        dialogList.add(dialog)
        dialog = Dialog(
            "AppTheme_Kudo_Dialog_Alert_HalfButton_GreenNegative",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_HalfButton_GreenNegative
        )
        dialogList.add(dialog)
        dialog = Dialog(
            "AppTheme_Kudo_Dialog_Alert_HalfButton_SingleLine",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_HalfButton_SingleLine
        )
        dialogList.add(dialog)
        dialog = Dialog(
            "AppTheme_Kudo_Dialog_Alert_StackedButton",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_StackedButton
        )
        dialogList.add(dialog)

        dialog = Dialog(
            "AppTheme_Kudo_Dialog_Alert_VerticalButton",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_VerticalButton
        )
        dialogList.add(dialog)

        dialog = Dialog(
            "AppTheme_Kudo_Dialog_Alert_Reversed",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_Reversed
        )
        dialogList.add(dialog)
        dialog = Dialog(
            "AppTheme_Kudo_Dialog_Alert_Reversed",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_VerticalButton_SingleLine
        )
        dialogList.add(dialog)
        dialog = Dialog(
            "R.style.AppTheme_Kudo_Dialog_Alert_Confirmation No Title",
            "",
            getString(R.string.sample_message),
            getString(R.string.yes),
            getString(R.string.no), R.style.AppTheme_Kudo_Dialog_Alert_Confirmation
        )
        dialogList.add(dialog)
        dialog = Dialog(
            "R.style.AppTheme_Kudo_Dialog_Alert_Confirmation No Negative",
            getString(R.string.sample_title),
            getString(R.string.sample_message),
            getString(R.string.yes),
            "", R.style.AppTheme_Kudo_Dialog_Alert_Confirmation
        )
        dialogList.add(dialog)
        adapter = ItemAdapter(dialogList, this)
    }

    fun invokeDialog(dialog: Dialog) {
        val confirmationDialogFragment = ConfirmationDialogFragment.create(
            dialog.title,
            dialog.message, dialog.positiveButton, dialog.negButton, dialog.customTheme
        )

        confirmationDialogFragment.setDialogButtonListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_NEGATIVE -> Log.v("test", "Negative")
                DialogInterface.BUTTON_POSITIVE -> Log.v("test", "Possitive")
                else -> Log.v("test", "Possitive")
            }
        }

        confirmationDialogFragment.show(supportFragmentManager, dialogTag)

    }
}

class ItemAdapter(val dialogList: List<Dialog>, val itemClick: ItemClickListener) :
    RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_dialog, p0, false))
    }

    override fun getItemCount(): Int {
        return dialogList.size
    }

    override fun onBindViewHolder(p0: ItemViewHolder, p1: Int) {
        p0.bind(dialogList[p1])
    }


    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(dialog: Dialog) {
            itemView.tv_item.text = dialog.styleName
            itemView.setOnClickListener { itemClick.onItemClick(dialog) }
        }

    }

    interface ItemClickListener {
        fun onItemClick(dialog: Dialog)
    }
}

data class Dialog(
    val styleName: String,
    val title: CharSequence = "",
    val message: CharSequence = "",
    val positiveButton: CharSequence = "",
    val negButton: CharSequence = "",
    val customTheme: Int
)

