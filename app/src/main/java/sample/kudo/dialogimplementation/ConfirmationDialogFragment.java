package sample.kudo.dialogimplementation;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import kudo.mobile.app.ui.KudoButton;

/**
 * Created by muh.rahmatullah on 21/03/19.
 */
public class ConfirmationDialogFragment extends AppCompatDialogFragment {

    CharSequence mTitle;

    CharSequence mMessage;

    CharSequence mPositiveBtn;

    CharSequence mNegativebtn;

    boolean mMaterial;

    int mDialogTheme;

    private DialogInterface.OnClickListener mDialogBtnListener;

    public void setDialogButtonListener(DialogInterface.OnClickListener dialogBtnListener) {
        mDialogBtnListener = dialogBtnListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (mMaterial) {
            AlertDialog dialog = new AlertDialog.Builder(getActivity(), mDialogTheme)
                    .setTitle(mTitle)
                    .setMessage(Html.fromHtml(mMessage.toString()))
                    .setNegativeButton(mNegativebtn, (dialog1, which) -> {
                        if (mDialogBtnListener != null) {
                            mDialogBtnListener.onClick(dialog1, which);
                        }
                    })
                    .setPositiveButton(mPositiveBtn, (dialog12, which) -> {
                        if (mDialogBtnListener != null) {
                            mDialogBtnListener.onClick(dialog12, which);
                        }
                    })
                    .create();

            if (mDialogBtnListener != null) {
                setCancelable(false);
            }

            return dialog;
        }
        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.confirmation_dialog_fragment, null, false);
        TextView titleTv = view.findViewById(R.id.dialog_message_tv_title);
        if (TextUtils.isEmpty(mTitle)) {
            titleTv.setVisibility(View.GONE);
        } else {
            titleTv.setText(mTitle);
        }

        TextView messageTv = view.findViewById(R.id.dialog_message_tv_message);
        if (TextUtils.isEmpty(mMessage)) {
            messageTv.setVisibility(View.GONE);
        } else {
            messageTv.setText(mMessage);
        }
        KudoButton positiveBtn = view.findViewById(R.id.dialog_message_btn_positive);
        positiveBtn.setText(mPositiveBtn);
        KudoButton negativeBtn = view.findViewById(R.id.dialog_message_btn_negative);
        if (mNegativebtn != null) {
            negativeBtn.setText(mNegativebtn);
        } else {
            negativeBtn.setVisibility(View.GONE);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(view);
        final AlertDialog dialog = builder.create();

        if (mDialogBtnListener != null) {
            setCancelable(false);
        }

        positiveBtn.setOnClickListener(v -> {
            dismiss();
            if (mDialogBtnListener != null) {
                mDialogBtnListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
            }
        });
        negativeBtn.setOnClickListener(v -> {
            dismiss();
            if (mDialogBtnListener != null) {
                mDialogBtnListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
            }
        });

        return dialog;
    }

    public static ConfirmationDialogFragment create(CharSequence title, CharSequence message,
                                                    CharSequence positiveBtn,
                                                    CharSequence negativeBtn) {
        return create(title, message, positiveBtn, negativeBtn, true, R.style
                .AppTheme_Kudo_Dialog_Alert, null);
    }

    public static ConfirmationDialogFragment create(CharSequence title, CharSequence message,
                                                    CharSequence positiveBtn,
                                                    CharSequence negativeBtn, DialogInterface.OnClickListener clickListener) {
        return create(title, message, positiveBtn, negativeBtn, true, R.style
                .AppTheme_Kudo_Dialog_Alert, clickListener);
    }

    public static ConfirmationDialogFragment create(CharSequence title, CharSequence message,
                                                    CharSequence positiveBtn,
                                                    CharSequence negativeBtn, int customTheme) {
        return create(title, message, positiveBtn, negativeBtn, true, customTheme, null);
    }

    private static ConfirmationDialogFragment create(CharSequence title, CharSequence message,
                                                     CharSequence positiveBtn,
                                                     CharSequence negativeBtn,
                                                     boolean material, int customTheme, DialogInterface.OnClickListener clickListener) {
        ConfirmationDialogFragment dialog = new ConfirmationDialogFragment();
        dialog.mTitle = title;
        dialog.mMessage = message;
        dialog.mPositiveBtn = positiveBtn;
        dialog.mNegativebtn = negativeBtn;
        dialog.mMaterial = material;
        dialog.mDialogTheme = customTheme;
        dialog.mDialogBtnListener = clickListener;
        return dialog;
    }
}
