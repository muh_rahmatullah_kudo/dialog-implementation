# Dialog Implementation

This is a sample app to showcase various theme of dialog implementation in kudo-ui module. 
This will support developer productivity to pick matched dialog theme from UI design.

When you run the app, you will be shown list of theme, when you click on of the list, 
it will shows you the design of the dialog. refer to the screenshots:



## Screenshots

### ps: to better see the demo, open image in new tab

![demo](screenshots/demo-dialog.gif)
